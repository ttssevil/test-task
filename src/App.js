import React from "react";
import Image from './image/Image' ; 
import Address from './address/Address' ;
import Price from './price/Price' ; 
import Area from './area/Area' ; 
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const URL = "http://demo4452328.mockable.io/" ;

const COMPONENTS = {
    IMAGE : <Image/> , 
    ADDRESS: <Address/> , 
    AREA: <Area/> , 
    PRICE: <Price/> 
}


const styles = theme => ({
    card: {
      maxWidth: 345,
    },
    media: {
      height: 140,
    },
    root:{
        flexGrow: 1 , 
        padding: "20px 15px "
    }
  });


class App extends React.Component{

    state = {
            templates : [] , 
            properties : [] , 
        }
        

    componentDidMount = () => {
        this.getProperties()
        this.getTemplates()
    }

    getProperties = () => {
        fetch(`${URL}properties`)
        .then(resp=>resp.json())
        .then(properties => this.setState({properties: properties.data}))
        .catch(error=>console.log("Error: " , error))
    }

    getTemplates = () => {
        fetch(`${URL}templates`)
        .then(resp=>resp.json())
        .then( templates=>this.setState({templates , template : templates[0].template }))
        .catch(error=>console.log("Error: " , error))
    }

    renderData = template => {
        const { classes } = this.props;
        return this.state.properties.map(prop => (
            <Grid item key={prop.id} lg={3} md={4}  sm={6} xs={6}>
                <Card className={classes.card}>
                    <CardActionArea>
                        <CardContent>
                            {template.map(t => this.renderElement(prop , t ))}
                        </CardContent>
                    </CardActionArea>
                </Card>
            </Grid> 
        ))
    }

    renderElement = (prop , temp)=>
        (
            React.cloneElement( 
                COMPONENTS[temp.component], 
                {
                    [temp.field]: prop[temp.field] , 
                    key: `${prop.id}_${temp.field}` , 
                }, 
                temp.children? temp.children.map(child=>this.renderElement(prop , child)) : null 
            )
        )

    renderTemplatePalette = templates => (
        <AppBar position="static">
            <Toolbar>
            {templates.map(temp => (
                <Button color="inherit" onClick={()=>this.setState({ template : temp.template })} key={temp.id}>{`Template № ${temp.id}`}</Button>
            ))}
            </Toolbar>
        </AppBar>
    )
       
    

    render(){
        const { properties ,  templates, template  } = this.state ; 
        const { classes } = this.props; 

        return(
            <React.Fragment>
                <CssBaseline />
                {templates.length ? this.renderTemplatePalette(templates) : null }
                <div className={classes.root}>
                    <Grid container spacing={16}>
                        {properties.length &&  templates.length ? this.renderData(template) : null }
                    </Grid>
                </div>
            </React.Fragment>
        )
    }
}


export default withStyles(styles)(App);