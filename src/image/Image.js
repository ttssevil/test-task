import React from 'react' ; 
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
      position: "relative"
    },
    gridList: {
      width: 500,
      height: 160,
    },
    media: {
        height: 140,
      },
  });

class Image extends React.Component {
  render() {
      const { images , children , classes } = this.props ; 
    return (
            <div className={classes.root}>
                <GridList cellHeight={160} className={classes.gridList} cols={images.length}>
                {images.map((img , index)  => (
                    <GridListTile key={index} cols={1}>
                    <img src={img} alt={"sdsd"} />
                    </GridListTile>
                ))}
                </GridList>
                {children}
            </div>
        )
    }
}





export default withStyles(styles)(Image) ; 

