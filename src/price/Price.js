import React from 'react' ; 
import Typography from '@material-ui/core/Typography';


const Price = props =>(
    <div>
        <Typography variant="subheading">{`Price: ${props.price}`}</Typography>
        {props.children}
    </div>
)

export default Price ; 