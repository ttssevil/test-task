import React from 'react' ; 
import Typography from '@material-ui/core/Typography';


const Address = props =>(
    <div>
        <Typography variant="subheading">{`Address is: ${props.full_address}`}</Typography>
        {props.children}
    </div> 
)

export default Address ; 