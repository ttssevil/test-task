import React from 'react' ; 
import Typography from '@material-ui/core/Typography';


const Area = props => (
    props.area 
        ? 
            <div>
                <Typography variant="subheading">{`Area is: ${props.area}`}</Typography>
                {props.children}
            </div>
        : null
)

export default Area ; 
